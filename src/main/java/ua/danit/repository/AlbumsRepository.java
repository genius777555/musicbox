package ua.danit.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;
import ua.danit.model.Album;


@Repository
public interface AlbumsRepository extends JpaRepository<Album, Long> {


    Album findByAlbumName(String album);
}
