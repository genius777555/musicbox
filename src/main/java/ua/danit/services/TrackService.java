package ua.danit.services;

import ua.danit.model.Track;

import java.util.List;


public interface TrackService {


    List<Track> getAll();

    Track getById(Long id);

    void deleteTrack(Long id);

    Track addTrack(Track track);

    Track updateTrack(Track track, Long id);

      Track uploadTrack(Track track);
}