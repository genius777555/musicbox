package ua.danit.services;

import org.springframework.stereotype.Service;
import ua.danit.model.Like;

import java.util.List;

@Service
public interface LikeService {

    List<Like> getAll();

    List<Like> getLikedById(Long id);

    void deleteLikeFromTrack(Long id);

    Like addLikeToTrack(Like like);

    Like updateLikedTrack(Like like,Long userId, Long track_id);

    String testing(Long userId);
}
