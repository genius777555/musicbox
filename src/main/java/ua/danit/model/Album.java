package ua.danit.model;


import com.fasterxml.jackson.annotation.*;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "albums_tt")
public class Album {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "singer")
    private String singerName;

    @Column(name = "albumName")
    private String albumName;

    @Column(name = "cover")
    private String coverPath;

    @OneToMany(mappedBy = "album")
    @JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
    private List<Track> tracks;
    public void setAlbumName(String name) {
        this.albumName = name;
    }

    public String getAlbumName() {
        return albumName;
    }


}
