package ua.danit.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ua.danit.model.User;
import ua.danit.repository.UserRepository;
import ua.danit.security.JwtUserFactory;



@Service
public class JwtUserDetailsService implements UserDetailsService {
   @Autowired
    private UserRepository userRepository;


    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        User user = userRepository.findByUserName(userName);

        if (user==null){
            System.out.println("User not found");
            throw new UsernameNotFoundException("Username not found");
        }
        return JwtUserFactory.create(user);
    }


}
