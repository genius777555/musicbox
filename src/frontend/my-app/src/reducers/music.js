
import {ADD_ALBUM, ADD_TRACKS} from "../constants/actionTypes";

import {DELETE_ALL_ALBUMS} from "../constants/actionTypes";


const initialState = {
    albums: [],
    tracks: [],
    audioPlayer: {
        enable : false
    }

};


export default function musicReducer (state = initialState, action) {
    if (action.type === ADD_ALBUM) {
        return {...state, albums: action.albums};
    } else if (action.type === DELETE_ALL_ALBUMS) {
        return {...state, albums: action.deleteAlbums}
    } else if (action.type === ADD_TRACKS) {
        return {...state, tracks: action.tracks}
    }

    return state;

}