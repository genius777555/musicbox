import {DELETE_CURRENT_USER, IS_AUTH, SHOW_CURRENT_USER} from "../constants/actionTypes";

const initialState = {
    user: undefined,
    isAuthenticated: false
};


export default function authenticationReducer (state = initialState, action) {
    if (action.type === IS_AUTH) {
        return {...state, isAuthenticated: action.authBool}
    } else if (action.type === SHOW_CURRENT_USER) {
        return {...state, user: action.user}
    } else if (action.type === DELETE_CURRENT_USER) {
        return {...state, user: action.deleteUser}
    }


    return state;

}