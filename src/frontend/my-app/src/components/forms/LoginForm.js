import React from 'react'
import { Field, reduxForm } from 'redux-form'

const validate = values => {
    const errors = {};
    if (!values.userName) {
        errors.userName = 'Required'
    } else if (values.userName.length > 15) {
        errors.userName = 'Must be 15 characters or less'
    }
    if (!values.password) {
        errors.password = 'Required'
    }
    return errors
};


const renderField = ({ input, label, type, meta: {touched, error} }) => (
    <div>
        <label>{label}</label>
        <div>
            <input {...input} placeholder={label} type={type}/>
            {touched && ((error && <span>{error}</span>))}
        </div>
    </div>
);

const LoginForm = (props) => {
    const { handleSubmit, pristine, reset, submitting } = props;
    return (
        <form onSubmit={handleSubmit}>
            <Field name="userName" type="text" component={renderField} label="Username"/>
            <Field name="password" type="password" component={renderField} label="Password"/>
            <div>
                <button type="submit" disabled={submitting}>Submit</button>
                <button type="button" disabled={pristine || submitting} onClick={reset}>Clear Values</button>
            </div>

        </form>
    )
}

export default reduxForm({
    form: 'syncValidation',  // a unique identifier for this form
    validate               // <--- validation function given to redux-form
    // <--- warning function given to redux-form
})(LoginForm)
