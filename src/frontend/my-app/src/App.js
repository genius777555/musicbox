import React, {Component} from 'react';
import logo from './new_logo.png';
import {Link, Route, Switch, withRouter} from 'react-router-dom'
import './App.css';
import Albums from "./containers/Albums";
import Admin from "./containers/Admin";
import Login from "./containers/Login";
import AlbumTracks from "./containers/AlbumTracks";
import routes from "./constants/routes";
import {addAlbums, addTracks, isAuth, putCurrentUser} from "./actions/actions";
import {connect} from "react-redux";
import Favorite from "./containers/Favorite";
import Logout from "./containers/Logout"
import {getAllAlbums, getAllTracks, getUser} from "./utils/Utils"
import User from "./containers/User"
import Play from "./containers/Play"


class App extends Component {
    componentDidMount() {

        getAllAlbums(data => {
            console.log('data', data);
            this.props.addAlbums(data);
            if (localStorage.getItem('token') != null) {
            this.props.isAuth(true);
                getUser(data => {
                    this.props.putCurrentUser(data);
                })}

        });
        getAllTracks(data => {
            this.props.addAllTracks(data);
        });
        console.log(document.body)
    }
    render() {

        return (
            <div className="body">
                <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lobster" />
                <img src={logo} className="App-logo" alt="logo"/>

                <header>
                    <nav>
                    <ul className="fancyNav">
                        <li id="home"><Link className="homeIcon" to={routes.home.href}>{routes.home.name}</Link></li>
                        <li id="news"><Link to={routes.albums.href}>{routes.albums.name}</Link></li>
                        <li id="about"><Link  to={routes.fav.href}>{routes.fav.name}</Link></li>
                        {this.props.isAuthorization && <li id="services">
                            <Link  to={routes.user.href}>{this.props.currentUser && this.props.currentUser.username}</Link></li>}
                        {!this.props.isAuthorization && <li id="services">
                            <Link to={routes.login.href}>{routes.login.name}</Link></li>}
                        {this.props.isAuthorization && <li id="services">
                            <Link  to={routes.logout.href}>{routes.logout.name}</Link></li>}
                         <li id="services"><Link to={routes.admin.href}>{routes.admin.name}</Link></li>
                    </ul>
                </nav>
                </header>
                <Switch>
                    <Route exact path="/albums" component={Albums}/>
                    <Route path="/login" component={Login}/>
                    <Route path="/albums/:albumId" component={AlbumTracks}/>
                    <Route path="/favorite" component={Favorite}/>
                    <Route path="/admin" component={Admin}/>
                    <Route path="/logout" component={Logout}/>
                    <Route path="/user" component={User}/>
                    <Route path="/play/:trackId" component={Play}/>
                </Switch>


            </div>)
    }
}
const mapStateToProps = (state) => {
    return {isAuthorization: state.authentication.isAuthenticated,
    currentUser: state.authentication.user}
};

const mapDispatchToProps = (dispatch) =>{
    return {
        addAlbums: (data) => {
            dispatch(addAlbums(data));
        },
        isAuth: (data) => {
            dispatch(isAuth(data))
        },
        putCurrentUser: (data) => {
            dispatch(putCurrentUser(data));
        },
        addAllTracks: (data) => {
            dispatch(addTracks(data))
        }
    }
};
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App))
