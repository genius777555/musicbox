import React, {Component} from 'react'
import {connect} from "react-redux";




class Play extends Component{

    render() {
        return (
            <div>
                <audio className="App-tracks" controls>
                <source src={this.props.currentTrack && this.props.currentTrack.trackPath}/>
                </audio>


            </div>
        )

    }

}

const mapStateToProps = (state, ownProps) => {
    return {currentTrack: state.music.tracks.find(track => track.id === +ownProps.match.params.trackId)}
};

export default connect(mapStateToProps)(Play)