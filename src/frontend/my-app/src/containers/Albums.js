import React, {Component} from 'react'

import routes from '../constants/routes'
import '../App.css';
import {Link} from 'react-router-dom'
import { connect } from 'react-redux'

class Albums extends Component {


    render(){
        return <div className="App-get-albums">
            {this.props.albums && this.props.albums.map(album =>

                    <li className="form-inline" key={album.id}>
                        <img className="Album-cover" src={album.coverPath} alt="cover"/>
                        <Link className="App-album-name form-inline" to={routes.tracks.href + album.id}>
                            {album.albumName}:
                            <div className="App-singer-name">{album.singerName}</div></Link>
                    </li>
                )}
        </div>
    }
}
const mapStateToProps = (state) => {
    return {albums: state.music.albums}
    };







export default connect(mapStateToProps)(Albums)