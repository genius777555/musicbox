



const routes = {
    home: {name: "Home", href: "/"},
    albums: {name: "All albums", href: "/albums"},
    login: {name: "login", href: "/login"},
    logout: {name: "logout", href: "/logout"},
    tracks: {name: "albumTracks", href: "/albums/"},
    fav: {name: "Favourite Tracks", href: "/favorite"},
    admin: {name: "Admin Panel", href: "/admin"},
    user: {href: "/user"},
    playMusic: {name: "play", href: "/play/"}
};



export default routes