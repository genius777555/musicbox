import settings from "../constants/settings";


export const getAllAlbums = (callback) =>{

    const url = settings.api + "/api/albums/";
    const params = {
        method: "GET",
        headers: {
            'Content-Type': 'application/json; charset=utf-8'},
        mode: 'cors'
    };
    fetch(url, params)
        .then(res => res.json())
        .then(callback)
};

export const  getUser = (callback) => {

    const url = settings.api + "/api/user/me";
    const params = {
        method: "GET",
        headers: {
            "Authorization": "Bearer " + localStorage.getItem('token'),
            'Content-Type': 'application/json; charset=utf-8'
        },
        credentials: 'include',
        mode: 'cors'
    };
    fetch(url, params)
        .then(res => res.json())
        .then(callback)

};

export const getAllTracks = (callback) => {
    const url = settings.api + "/api/tracks/";
    const params = {
        method: "GET",
        headers: {
            'Content-Type': 'application/json; charset=utf-8'},
        mode: 'cors'
    };
    fetch(url, params)
        .then(res => res.json())
        .then(callback)
};


